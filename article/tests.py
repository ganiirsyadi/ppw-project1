from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import *
import json
import time
from django.contrib.auth.models import User
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_add_transaction(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/register')
        # find the register input and button
        fullname = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_first_name"]')))
        username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
        password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
        password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
        register = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/button')))
        # fill the input
        fullname.send_keys("Lucky Susanto")
        username.send_keys("monyetterbang")
        password1.send_keys("terbangbersamaku123")
        password2.send_keys("terbangbersamaku123")
        # click the register button
        register.click()
        # wait until redirected to login page (the login input and button exist)
        login_username = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[1]/input')))
        login_password = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[2]/input')))
        login_button = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/button')))
        # fill the input
        login_username.send_keys("monyetterbang")
        login_password.send_keys("terbangbersamaku123")
        login_button.click()
        # It should redirected to index
        # switch to article page
        navbar_article = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[5]/a')))
        navbar_article.click()
        # locate the input element
        input_title = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_title"]')))
        input_motto = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_motto"]')))
        input_image = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_image"]')))
        input_content = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_content"]')))
        submit_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="submit"]')))
        input_title.send_keys("Bandung")
        input_motto.send_keys("Kota Romantis")
        input_image.send_keys("https://awsimages.detik.net.id/visual/2020/04/19/526b64df-6221-445e-8f40-7d9602037501_169.jpeg?w=650")
        input_content.send_keys("  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime tempore, alias nulla porro dolorem odio velit quas sequi mollitia, quia aut pariatur cupiditate repellendus beatae ipsa culpa. Optio dolorum nihil in at reiciendis omnis iste reprehenderit, quia dignissimos enim doloribus commodi aut excepturi debitis aperiam expedita numquam et vitae eius.")
        submit_button.click()
        # locate card for article just made
        article_card = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[2]/div[1]/div[1]/div/a')))
        article_card.click()
        # redirected to article detail
        score_select = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="score"]/option[3]')))
        score_select.click()
        submit_score = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="Post"]/div/button')))
        submit_score.click()
        # change score
        score_select = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="score"]/option[2]')))
        score_select.click()
        submit_score = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="Post"]/div/button')))
        submit_score.click()

        time.sleep(1)
        self.assertIn("monyetterbang", selenium.page_source)
