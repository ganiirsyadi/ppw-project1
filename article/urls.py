from django.urls import path

from . import views

app_name = "article"

urlpatterns = [
    path('', views.index, name='article'),
    path('detail/<str:pk>', views.detail, name='detail'),
    path('add/', views.add, name='add'),
    path('get_all/', views.get_articles, name='get'),
    path('add_review/<str:pk>', views.add_review, name='add_review'),
    path('get_review/<str:pk>', views.get_review, name='get_review'),
    path('get_score/<str:pk>', views.get_score, name='get_score')
]