from django.shortcuts import render, redirect
from .models import Article, ArticleReview
from . import forms
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url="autentikasi:loginPage")
def index(request):
    VarA = forms.formulir()
    VarA_dictio = {
        'formulir' : VarA,
    }
    return render(request, 'article/index.html', VarA_dictio)

@login_required(login_url="autentikasi:loginPage")
def detail(request, pk):
    article = Article.objects.get(id=pk)
    context = {'article' : article}
    return render(request, 'article/detail.html', context)

@login_required(login_url="autentikasi:loginPage")
def add(request):
    if request.method == "POST":
        form = forms.formulir({
            'author' : request.user,
            'title' : request.POST['title'],
            'motto' : request.POST['motto'],
            'image' : request.POST['image'],
            'content' : request.POST['content'],
        })
        if(form.is_valid()):
            form.save()
        return JsonResponse({'status' : 'success'})

@login_required(login_url="autentikasi:loginPage")
def get_articles(request):
    articles = Article.objects.all()
    article_list = serializers.serialize('json', articles)
    return HttpResponse(article_list, content_type="text/json-comment-filtered")

@login_required(login_url="autentikasi:loginPage")
def get_score(request, pk):
    article = Article.objects.get(id=pk)
    review = ArticleReview.objects.filter(article=article)
    score = "Unreviewed"
    if len(review) > 0:
        score = 0
        for obj in review:
            score+=obj.score
        score = round(score/len(review),2)
    return JsonResponse({'score' : score})

@login_required(login_url="autentikasi:loginPage")
def add_review(request, pk):
    if request.method == "POST":
        article = Article.objects.get(id=pk)
        user = request.user
        score = request.POST["score"]
        try:
            review = ArticleReview.objects.get(article=article, user=request.user)
            review.score = score
            review.save()
        except:
            article = ArticleReview.objects.create(
                article = article,
                user = user,
                score = score
            )
        return JsonResponse({'status' : 'success'})  
        
@login_required(login_url="autentikasi:loginPage")
def get_review(request,pk):
    article = Article.objects.get(id=pk)
    reviews = ArticleReview.objects.filter(article=article)
    result = []
    for obj in reviews:
        print(result.append({"user":str(obj.user), "score":obj.score}))
    return JsonResponse({"result":result})
