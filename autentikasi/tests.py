from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .views import *
from .forms import CreateUserForm
import time
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.

class ModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            username = 'popol',
            password = 'kurakuraninja',
        )
    def test_instance_created(self):
        self.assertEqual(User.objects.count(), 1)

class UrlsTest(TestCase):

    def setUp(self):
        self.login = reverse("autentikasi:loginPage")
        self.register = reverse("autentikasi:registerPage")
        self.logout = reverse("autentikasi:logoutPage")
    
    def test_login_use_right_function(self):
        found = resolve(self.login)
        self.assertEqual(found.func, loginPage)

    def test_register_use_right_function(self):
        found = resolve(self.register)
        self.assertEqual(found.func, registerPage)

    def test_logout_use_right_function(self):
        found = resolve(self.logout)
        self.assertEqual(found.func, logoutPage)

class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.login = reverse("autentikasi:loginPage")
        self.register = reverse("autentikasi:registerPage")
        self.logout = reverse("autentikasi:logoutPage")

    def test_GET_register(self):
        response = self.client.get(self.register)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "autentikasi/register.html")
        self.assertIn("Register", str(response.content))

    def test_POST_register_invalid(self):
        response = self.client.post(self.register, {
            'username': 'ganiirsyadi',
            'password1': 'ga',
            'password2': 'ga'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "autentikasi/register.html")
        self.assertIn("This password is too short", str(response.content))

    def test_POST_register_valid(self):
        response = self.client.post(self.register, {
            'username': 'ganiirsyadi',
            'password1': 'awertyuh67',
            'password2': 'awertyuh67'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "autentikasi/login.html")
        self.assertIn("Your account created successfully, please login", str(response.content))

    def test_GET_login(self):
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "autentikasi/login.html")
        self.assertIn("Welcome Back,", str(response.content))

    def test_POST_login_invalid(self):
        response = self.client.post(self.login, {
            'username': 'ganiirsyadi',
            'password': 'passwordsalah',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "autentikasi/login.html")
        self.assertIn("Username or password is incorrect", str(response.content))

    def test_POST_login_valid(self):
        user = User.objects.create(username='ganiirsyadi')
        user.set_password('awertyuh67')
        user.save()
        response = self.client.post(self.login, {
            'username': 'ganiirsyadi',
            'password': 'awertyuh67',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "homepage/index.html")
        self.assertIn("ganiirsyadi", str(response.content))

    def test_GET_logout(self):
        # If a user login, this view should logout the user and redirect to loginpage
        user = User.objects.create(username='kukang')
        user.set_password('12345')
        user.save()
        self.client.login(username='kukang', password='12345')      
        response = self.client.get(self.logout, follow=True) 
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "autentikasi/login.html")
        self.assertIn("Logout successfully", str(response.content))

class FormTest(TestCase):

    def test_form_is_valid(self):
        form = CreateUserForm(data = {
            "username" : "koala",
            "first_name" : "koala kumal",
            "email": "koala@gmail.com",
            "password1": "australia345",
            "password2": "australia345"
        })
        self.assertTrue(form.is_valid())
    
    def test_form_invalid(self):
        form = CreateUserForm(data = {})
        self.assertFalse(form.is_valid())

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_register_invalid(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/register')
        # find the register input and button
        username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
        password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
        password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
        register = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        # fill the input
        username.send_keys("monyetterbang")
        password1.send_keys("123")
        password2.send_keys("123")
        # click the register button
        register.click()
        # wait for a while
        time.sleep(1)
        # check error message exist
        self.assertIn("This password is too short", selenium.page_source)

    def test_register_valid_login_logout(self):
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/register')
        # find the register input and button
        username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
        password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
        password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
        register = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        # fill the input
        username.send_keys("monyetterbang")
        password1.send_keys("terbangbersamaku123")
        password2.send_keys("terbangbersamaku123")
        # click the register button
        register.click()
        # wait until redirected to login page (the login input and button exist)
        login_username = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[1]/input')))
        login_password = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[2]/input')))
        login_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        # fill the input
        login_username.send_keys("monyetterbang")
        login_password.send_keys("terbangbersamaku123")
        login_button.click()
        # It should redirected to index
        # wait until logout button exist
        logout_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.nav-item .logout-btn')))
        # Check name of the user exist
        self.assertIn("monyetterbang", selenium.page_source)
        # logout
        logout_button.click()
        # It should redirected back to login
        # Check login_button exist
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        self.assertIn("Login", selenium.page_source)
