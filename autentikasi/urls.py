from django.urls import path

from . import views

app_name = "autentikasi"

urlpatterns = [
    path('login', views.loginPage, name='loginPage'),
    path('register', views.registerPage, name='registerPage'),
    path('logout', views.logoutPage, name='logoutPage'),
]