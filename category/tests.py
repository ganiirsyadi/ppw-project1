from django.test import TestCase,Client
from django.urls import resolve
from .views import index
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from homepage.models import Mobil
from category.models import Kategori
from django.contrib.auth.models import User
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By



class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium	
        wait = WebDriverWait(selenium, 5)
        selenium.get(self.live_server_url+'/register')
        username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
        password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
        password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
        register = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        username.send_keys("Tio")
        password1.send_keys("Tiojuga202")
        password2.send_keys("Tiojuga202")
        register.click()

        time.sleep(1)
        name=selenium.find_element_by_name("username")
        password=selenium.find_element_by_name("password")
        kirim=selenium.find_element_by_id("submit")
        name.send_keys('Tio')
        password.send_keys('Tiojuga202')
        kirim.send_keys(Keys.RETURN)
        time.sleep(1)
        cektex= self.selenium.find_element_by_tag_name('body').text

        self.assertIn('About Us', cektex)

        

        Kategori.objects.create(kategori="MPV")
        kategori=Kategori.objects.get(kategori="MPV")
        Mobil.objects.create(nama_mobil="Avanza",deskripsi="Kenceng",tahun=2010,harga=2000,kota="DKI",kategori=kategori)
        self.assertEqual("MPV",str(kategori))

        self.selenium.get(self.live_server_url + '/category/')


        time.sleep(1)
        # find the form element
        searchbox = selenium.find_element_by_name('input')
        subbut = selenium.find_element_by_name('submit')

        searchbox.send_keys("avanza")
        time.sleep(1)
        subbut.send_keys(Keys.RETURN)
        searchbox.send_keys("2000")
       
 

       

# Create your tests here
class unitTest(TestCase):
    def test_app_url_exist(self):
        response=Client().get('/category/')
        self.assertEqual(response.status_code,302)
    def test_view_def_exist(self):
        found=resolve('/category/')
        self.assertEqual(found.func, index)
    


    
