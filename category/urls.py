from django.urls import path

from . import views

app_name = "category"

urlpatterns = [
    path('', views.index, name='category'),
    path('cari', views.cari,name='cari')
]