from django.shortcuts import render
from homepage.models import Mobil
from homepage.forms import formulir
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required(login_url="autentikasi:loginPage")
def index(request):
    return render(request,'category/index.html')

@login_required(login_url="autentikasi:loginPage")
def cari(request):
    q=request.GET['q']
    showroom=[]
    mobils= Mobil.objects.all()
    for i in mobils:
        try:
            price=int(q)
            if(price>=int(i.harga)):
                showroom.append({
                'nama':i.nama_mobil,
                'harga':i.harga,
                'foto': i.foto,
                'kota':i.kota
            })
        except:
            if(q.lower()==i.nama_mobil.lower()):
                showroom.append({
                    'nama':i.nama_mobil,
                    'harga':i.harga,
                    'foto': i.foto,
                    'kota':i.kota
                })
    data={
        'cars' :showroom
    }

    return JsonResponse(data, safe=False)


