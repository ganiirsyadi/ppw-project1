from django import forms
from .models import Mobil

class formulir(forms.ModelForm):
    class Meta:
        model = Mobil
        fields = "__all__"
        widgets = {
            'nama_mobil' : forms.TextInput(attrs={'class': 'form-control'}),
            'kategori' : forms.Select(attrs={'class': 'form-control'}),
            'deskripsi' : forms.TextInput(attrs={'class': 'form-control'}),
            'tahun' : forms.NumberInput(attrs={'class': 'form-control'}),
            'harga' : forms.NumberInput(attrs={'class': 'form-control'}),
            'foto' : forms.TextInput(attrs={'class': 'form-control'}),
            'kota' : forms.TextInput(attrs={'class': 'form-control'}),
        }

