from django.db import models
from category.models import Kategori
from django.contrib.auth.models import User

# Create your models here.
class Mobil(models.Model):
    nama_mobil = models.CharField(blank=False, max_length= 100)
    kategori = models.ForeignKey(Kategori, on_delete=models.CASCADE)
    deskripsi = models.CharField(blank=False, max_length= 200)
    tahun = models.IntegerField(blank=False)
    harga = models.IntegerField(blank=False)
    kota = models.CharField(blank=False,max_length=100)
    foto = models.CharField(max_length=400)
    def __str__(self):
        return self.nama_mobil

class FavoriteCars(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cars = models.ManyToManyField(Mobil)
    def __str__(self):
        return self.user.username