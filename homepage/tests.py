from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .models import Mobil, FavoriteCars
from category.models import Kategori
from .views import *
import json
import time
from django.contrib.auth.models import User
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains


class ModelTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            username = 'lucky',
            password = 'ducky',
        )
        self.kategori = Kategori.objects.create(
            kategori = 'SCV'
        )
        self.mobil = Mobil.objects.create(
            nama_mobil = 'Avanza',
            kategori = self.kategori,
            deskripsi = 'A good car',
            tahun = 2020,
            harga = 1500000,
            kota = 'Jakarta',
            foto = 'https://www.semisena.com/wp-content/uploads/2020/02/Toyota-Avanza-F.jpg'
        )
        self.favoriteCars =  FavoriteCars.objects.create(
            user = self.user,
        )
        self.favoriteCars.cars.add(self.mobil)
        self.favoriteCars.save()

    def test_instance_created(self):
        self.assertEqual(Mobil.objects.count(), 1)
        self.assertEqual(FavoriteCars.objects.count(), 1)
        
    def test_model_mobil_exist(self):
        self.assertEqual(str(self.mobil), 'Avanza')
        
    def test_model_favcar_exist(self):
        self.assertEqual(str(self.favoriteCars), 'lucky')

class UrlsTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(
            username = 'lucky',
            password = 'ducky',
        )
        self.kategori = Kategori.objects.create(
            kategori = 'SCV'
        )
        self.mobil = Mobil.objects.create(
            nama_mobil = 'Avanza',
            kategori = self.kategori,
            deskripsi = 'A good car',
            tahun = 2020,
            harga = 1500000,
            kota = 'Jakarta',
            foto = 'https://www.semisena.com/wp-content/uploads/2020/02/Toyota-Avanza-F.jpg'
        )
        self.favoriteCars =  FavoriteCars.objects.create(
            user = self.user,
        )
        self.favoriteCars.cars.add(self.mobil)
        self.favoriteCars.save()
        self.homepage = reverse("homepage:homepage")
        self.getfavcars = reverse("homepage:getfavcars")
        self.favcars = reverse("homepage:favcars")
        self.addcars = reverse("homepage:addcars")
        self.delcars = reverse("homepage:delcars", args=['1'])
        
    def test_homepage_use_right_function(self):
        found = resolve(self.homepage)
        self.assertEqual(found.func, homepage)
    
    def test_getfavcars_use_right_function(self):
        found = resolve(self.getfavcars)
        self.assertEqual(found.func, get_favorite_cars)

    def test_favcars_use_right_function(self):
        found = resolve(self.favcars)
        self.assertEqual(found.func, favcars)
        
    def test_addcars_use_right_function(self):
        found = resolve(self.addcars)
        self.assertEqual(found.func, add)
        
    def test_deletefavcars_use_right_function(self):
        found = resolve(self.delcars)
        self.assertEqual(found.func, deleteFavCar)

class ViewsTest(TestCase):

    def setUp(self):
        self.kategori = Kategori.objects.create(
            kategori = 'SCV'
        )
        self.mobil = Mobil.objects.create(
            nama_mobil = 'Avanza',
            kategori = self.kategori,
            deskripsi = 'A good car',
            tahun = 2020,
            harga = 1500000,
            kota = 'Jakarta',
            foto = 'https://www.semisena.com/wp-content/uploads/2020/02/Toyota-Avanza-F.jpg'
        )
        self.mobil2 = Mobil.objects.create(
            nama_mobil = 'Avanza',
            kategori = self.kategori,
            deskripsi = 'A good car',
            tahun = 2020,
            harga = 1500000,
            kota = 'Jakarta',
            foto = 'https://www.semisena.com/wp-content/uploads/2020/02/Toyota-Avanza-F.jpg'
        )
        self.user = User.objects.create(username='kukang')
        self.user.set_password('12345')
        self.user.save()
        self.client = Client()
        self.homepage = reverse("homepage:homepage")
        self.getfavcars = reverse("homepage:getfavcars")
        self.favcars = reverse("homepage:favcars")
        self.addcars = reverse("homepage:addcars")
        self.delcars = reverse("homepage:delcars", args=['1'])


    def test_GET_homepage_no_login(self):
        response = self.client.get(self.homepage, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn("About Us", str(response.content))
        self.assertTemplateUsed("homepage/index.html")

    def test_POST_add(self):
        # Login a user
        self.client.login(username='kukang', password='12345')
        response = self.client.post(self.addcars, 
        {
            'car_id': '1',
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {'status' : 'success'})
        response2 = self.client.post(self.addcars, 
        {
            'car_id': '2',
        }, follow=True)
        self.assertEqual(response2.status_code, 200)
        self.assertJSONEqual(response2.content, {'status' : 'success'})

    def test_GET_getfavcars(self):
        # Login a user
        self.client.login(username='kukang', password='12345')
        self.client.post(self.addcars, 
        {
            'car_id': '1',
        }, follow=True)
        response = self.client.get(self.getfavcars)
        self.assertIn("Avanza", str(response.content))

    def test_GET_favcars(self):
        # Login a user
        self.client.login(username='kukang', password='12345')
        response = self.client.get(self.favcars)
        self.assertEqual(response.status_code, 200)
        self.assertIn("Your Favorite Cars", str(response.content))
        self.assertTemplateUsed("homepage/page2.html")

    def test_POST_delcars(self):
        # Login a user
        self.client.login(username='kukang', password='12345')
        self.client.post(self.addcars, 
        {
            'car_id': '1',
        }, follow=True)
        response = self.client.get(self.getfavcars)
        self.assertIn("Avanza", str(response.content))
        self.client.post(self.delcars)
        response2 = self.client.get(self.getfavcars)
        self.assertNotIn("Avanza", str(response2.content))

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_add_fav_cars(self):
        self.kategori = Kategori.objects.create(
            kategori = 'SCV'
        )
        self.mobil = Mobil.objects.create(
            nama_mobil = 'Avanza',
            kategori = self.kategori,
            deskripsi = 'A good car',
            tahun = 2020,
            harga = 1500000,
            kota = 'Jakarta',
            foto = 'https://www.semisena.com/wp-content/uploads/2020/02/Toyota-Avanza-F.jpg'
        )
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/register')
        # find the register input and button
        fullname = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_first_name"]')))
        username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
        password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
        password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
        register = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/button')))
        # fill the input
        fullname.send_keys("Lucky Susanto")
        username.send_keys("monyetterbang")
        password1.send_keys("terbangbersamaku123")
        password2.send_keys("terbangbersamaku123")
        # click the register button
        register.click()
        # wait until redirected to login page (the login input and button exist)
        login_username = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[1]/input')))
        login_password = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[2]/input')))
        login_button = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/button')))
        # fill the input
        login_username.send_keys("monyetterbang")
        login_password.send_keys("terbangbersamaku123")
        login_button.click()
        # It should redirected to index
        car_card = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[3]/div[2]/div/div/div/div')))
        self.selenium.execute_script("window.scrollTo(0, 1080)")
        like_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.like-car')))
        time.sleep(1)
        like_button.click()
        # switch to fav cars page
        navbar_favcars = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[2]/a')))
        time.sleep(2)
        self.assertIn("Avanza", selenium.page_source)
        # locate logout button
        logout_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'logout-btn')))
        # logout
        logout_button.click()
        # It should redirected back to login
        # Check login_button exist
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        self.assertIn("Login", selenium.page_source)
