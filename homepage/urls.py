from django.urls import path

from . import views

app_name = "homepage"

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('getFavCars/', views.get_favorite_cars, name='getfavcars'),
    path('favcars/', views.favcars, name='favcars'),
    path('addFavCar/', views.add, name='addcars'),
    path('deleteFavCar/<str:car_id>', views.deleteFavCar, name='delcars')
]
