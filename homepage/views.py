from django.shortcuts import render,redirect
from . import forms, models
from .models import FavoriteCars, Mobil
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.contrib.auth.decorators import login_required

# Create your views here.
def homepage(request):
    mobil = models.Mobil.objects.all()
    context = {
        'mobil' : mobil
    }
    return render(request, 'homepage/index.html', context)

@login_required(login_url="autentikasi:loginPage")
def add(request):
    if request.method == "POST":
        car_id = request.POST["car_id"]
        car = Mobil.objects.get(id=car_id)
        try:
            favCars = FavoriteCars.objects.get(user=request.user)
            favCars.cars.add(car)
            favCars.save()
        except Exception as e:
            favCars = FavoriteCars.objects.create(
                user = request.user,
            )
            favCars.cars.add(car)
            favCars.save()
        return JsonResponse({'status' : 'success'})

@login_required(login_url="autentikasi:loginPage")
def get_favorite_cars(request):
    cars = FavoriteCars.objects.get(user=request.user)
    cars_list = serializers.serialize('json', cars.cars.all())
    return HttpResponse(cars_list, content_type="text/json-comment-filtered")

@login_required(login_url="autentikasi:loginPage")
def favcars(request):
    return render(request, 'homepage/page2.html')

@login_required(login_url="autentikasi:loginPage")
def deleteFavCar(request, car_id):
    if request.method == "POST":
        car = Mobil.objects.get(id=car_id)
        favCars = FavoriteCars.objects.get(user=request.user)
        favCars.cars.remove(car)
        favCars.save()
        return JsonResponse({'status' : 'success'})

