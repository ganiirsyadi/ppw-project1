$('.btn-add-article').click(() => {    
    $.post($("meta[name='url_add']").attr("content"), {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
        title : $("#id_title").val(),
        motto : $("#id_motto").val(),
        image : $("#id_image").val(),
        content : $("#id_content").val(),
    })
    .done(() => {
        $("#id_title").val(""),
        $("#id_motto").val(""),
        $("#id_image").val(""),
        $("#id_content").val(""),
        getData()
    })
    .fail((e) => console.log(e.responseText))
})
$(document).ready(() => getData())
// Functions

function generateRow(data, score) {
    article = data.fields
    link = $("meta[name='url_base']").attr("content") + "detail/" + data.pk
    return `<div class="col-12 col-md-4 d-flex justify-content-center">
    <div class="card">
      <a href="${link}">
        <img class="img-responsive card-img-top" src="${article.image}">
        <div class="container kartu" style="background-color: lightgray">
          <h4 style="color: darkred"><b>${article.title}</b></h4>
          <p style="color: black">${article.date}</p>
          <p style="color: black">score : ${score} ${score == "Unreviewed" ? '' : '/5'}</p>
        </div>
      </a>
    </div>
  </div>`
}

function getData() { 
    $.get($("meta[name='url_get']").attr("content"))
    .done((results) => {                
        let content = $(".article-container")
        content.empty()
        if (results.length == 0) {
            content.append("<h2 class='mx-3'>Be the first to write an article here</h2>")
        } else {
            let articles = results
            articles.forEach(a => {                
                $.get($("meta[name='url_base']").attr("content")+"get_score/"+a.pk)
                .done((e) => {
                    content.append(generateRow(a,e.score))
                })
                .fail(e => console.log(e.responseText)
                )
            })
        }
    }) 
    .fail((e) => console.log(e))
}
