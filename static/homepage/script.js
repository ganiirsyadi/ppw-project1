$(".like-car").click(function () {
    let car_id = $(this).data("car")
    $.post($("meta[name='url_add']").attr("content"), {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
        car_id : car_id,
    })
    .done(() => {
        $(this).hide()
        $(this).next().show()
    })
    .fail((e) => {
        console.log(e.responseText);
    })
})

$(document).ready(() => {
    $.get($("meta[name='url_get']").attr("content"))
    .done((result) => {
            result.forEach((car) => {
                $(`#like-${car.pk}`).hide()
                $(`#unlike-${car.pk}`).show()
            })
        })
        
    })

$(".unlike-car").click(function () {
    let car_id = $(this).data("car")
    $.post($("meta[name='url_del']").attr("content") + car_id, {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
    })
    .done(() => {
        $(this).prev().show()
        $(this).hide()
    })
    .fail((e) => {
        console.log(e.responseText);
    })
})