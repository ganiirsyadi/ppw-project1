$(document).ready(() => {
    getData()
})

// Functions

function generateRow(data) {
    car = data.fields
    return `    
    <div class="col-md-4 pt-5">
    <div class="card" style="width: 18rem;">
      <img class="card-img-top" src="${car.foto}" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title">${car.nama_mobil}</h5>
        <p class="card-text">Ave Rp ${car.harga} / hari</p>
        <p class="btn btn-warning m-0 unlike-car" data-car="${data.pk}" id="unlike-${data.pk}">delete</p>
      </div>
    </div>
  </div>
`
}

function getData() { 
    $.get($("meta[name='url_get']").attr("content"))
    .done((cars) => {   
        let content = $(".fav-cars")
        content.empty()
        if (cars.length == 0) {
            content.append("<h2 class='mt-5'>Looks like you haven't chosen a car</h2>")
        } else {
            cars.forEach((e) => content.append(generateRow(e)))
        }
        $(".unlike-car").show()
    })
    .fail(() => {
        let content = $(".fav-cars")
        content.append("<h2 class='mt-5'>Looks like you haven't chosen a car</h2>")
    }) 
}

$(document).on("click", ".unlike-car", function () {
    let car_id = $(this).data("car")
    $.post($("meta[name='url_del']").attr("content") + car_id, {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
    })
    .done(() => {
        getData()        
    })
    .fail((e) => {
        console.log(e.responseText);
    })
})