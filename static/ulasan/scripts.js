$('.btn-submit').click(() => {    
    $.post($("meta[name='url_add']").attr("content"), {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
        rating : $("#id_rating").val(),
        review : $("#id_review").val(),
        car_id : $("#id_mobil").val(),
    })
    .done(() => {
        getData()
    })
})

$(document).ready(() => getData())

$(document).on("click", ".btn-delete", function() {
    console.log($(this).data("review"))
    $.post($("meta[name='url_base']").attr("content") + "delete/", {
        csrfmiddlewaretoken : $("meta[name='csrf']").attr("content"),
        car_id : $(this).data("review")
    })
    .done(() => {
        getData()
    })
}
)
// Functions

function generateRow(data, user) {
    review = data
    foto = $("meta[name='profile_photo']").attr("content")
    deleteButton = ""
    if (review.username == user) {
        deleteButton = `<button class="btn btn-secondary btn-delete" data-review="${review.id}">delete</button>`
    }
    return `<div class="card mb-4">
    <div class="row m-0">
        <div class="col-3 d-flex flex-column p-0 card-in">
            <img src="${foto}" class="person-img">
            <h5 class="mt-3 mb-0 text-center person-name text-white">${review.username}</h5>
            <h4 class="mt-1 text-center person-name text-white">${review.rating} / 5</h4>
        </div>
        <div class="col-6 border-left bg-light border-right d-flex align-items-center p-3 border-dark">
            <h5>${review.review}</h5>
        </div>
        <div class="col-3 bg-light d-flex align-items-center flex-column justify-content-center">
            <img src="${review.mobil}" class="car-img mb-2">
            ${deleteButton}
        </div>
    </div>
</div>`
}

function getData() { 
    $.get($("meta[name='url_get']").attr("content"))
    .done((results) => {                
        let content = $(".review-content")
        content.empty()
        if (results.length == 0) {
            content.append("<h2 class='mx-3'>Be the first to write a review here</h2>")
        } else {
            let articles = results.result
            let user = results.user
            articles.forEach(a => {                
                content.append(generateRow(a, user))
            })
        }
    }) 
}
