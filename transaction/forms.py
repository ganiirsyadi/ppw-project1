from django import forms
from .models import Transaction

class TransactionForm(forms.ModelForm):
    class Meta:
        model = Transaction
        fields = "__all__"
        widgets = {
            'username' : forms.TextInput(attrs={'class': 'form-control'}),
            'mobil' : forms.Select(attrs={'class': 'form-control'}),
            'lamaSewa' : forms.NumberInput(attrs={
                'class': 'form-control',
                'placeholder': '... hari'
            }),
            'tanggal' : forms.DateInput(attrs={'class': 'form-control', 'type':'date'}),
        }

