from django.db import models
from homepage.models import Mobil
from django.contrib.auth.models import User

# Create your models here.

class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    mobil = models.ForeignKey(Mobil, on_delete=models.CASCADE)
    lamaSewa = models.PositiveIntegerField()
    tanggal = models.DateField()