from django.shortcuts import render, redirect   
from .forms import TransactionForm
from .models import Transaction
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url="autentikasi:loginPage")
def transaction(request):
    if(request.method == "POST"):
        form = TransactionForm({
            'user' : request.user,
            'mobil' : request.POST['mobil'],
            'lamaSewa' : request.POST['lamaSewa'],
            'tanggal' : request.POST['tanggal']
        })
        if(form.is_valid()):
            form.save()
        return redirect('transaction:transaction')
    else:
        form = TransactionForm()
        transaction = Transaction.objects.all()
        context = {
            'form' : form,
            'transaction' : transaction
        }
        return render(request, 'transaction/index.html', context)
