from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import *
import json
import time
from homepage.models import Mobil
from category.models import Kategori
from django.contrib.auth.models import User
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains


class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument("--window-size=1920,1080");
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()
    
    def test_add_transaction(self):
        self.kategori = Kategori.objects.create(
            kategori = 'SCV'
        )
        self.mobil = Mobil.objects.create(
            nama_mobil = 'Avanza',
            kategori = self.kategori,
            deskripsi = 'A good car',
            tahun = 2020,
            harga = 1500000,
            kota = 'Jakarta',
            foto = 'https://www.semisena.com/wp-content/uploads/2020/02/Toyota-Avanza-F.jpg'
        )
        selenium = self.selenium
        wait = WebDriverWait(selenium, 5)
        # opening the link we want to test
        selenium.get(self.live_server_url+'/register')
        # find the register input and button
        fullname = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_first_name"]')))
        username = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_username"]')))
        password1 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password1"]')))
        password2 = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_password2"]')))
        register = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/button')))
        # fill the input
        fullname.send_keys("Lucky Susanto")
        username.send_keys("monyetterbang")
        password1.send_keys("terbangbersamaku123")
        password2.send_keys("terbangbersamaku123")
        # click the register button
        register.click()
        # wait until redirected to login page (the login input and button exist)
        login_username = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[1]/input')))
        login_password = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/div[2]/input')))
        login_button = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div/form/button')))
        # fill the input
        login_username.send_keys("monyetterbang")
        login_password.send_keys("terbangbersamaku123")
        login_button.click()
        # It should redirected to index
        # switch to article page
        navbar_review = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="navbarSupportedContent"]/ul/li[4]/a')))
        navbar_review.click()
        # locate the input element
        input_rating = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_rating"]')))
        input_review = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_review"]')))
        input_mobil = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="id_mobil"]/option[2]')))
        submit_button = wait.until(EC.element_to_be_clickable((By.XPATH, '//*[@id="review-form"]/div[4]/button')))
        input_rating.send_keys("4")
        input_review.send_keys("Nyaman")
        input_mobil.click()
        submit_button.click()
        # delete a review
        delete_button = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.btn-delete')))
        delete_button.click()
        # locate logout button
        logout_button = wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'logout-btn')))
        # logout
        logout_button.click()
        # It should redirected back to login
        # Check login_button exist
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'btn-auth')))
        self.assertIn("Login", selenium.page_source)