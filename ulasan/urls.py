from django.urls import path

from . import views

app_name = "ulasan"

urlpatterns = [
    path('', views.index, name='review'),
    path('add/', views.add, name='add'),
    path('get_all/', views.get_all, name='get'),
    path("delete/", views.delete, name="delete")
]