from django.shortcuts import render, redirect   
from .forms import ReviewForm
from .models import Review
from homepage.models import Mobil
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required(login_url="autentikasi:loginPage")
def index(request):
    form = ReviewForm()
    context = {
        'form' : form,
    }
    return render(request, 'ulasan/index.html', context)

@login_required(login_url="autentikasi:loginPage")
def add(request):
    if(request.method == "POST"):
        mobil = Mobil.objects.get(id=request.POST['car_id'])
        form = ReviewForm({
            'username' : request.user,
            'rating' : request.POST['rating'],
            'review' : request.POST['review'],
            'mobil' : mobil
        })
        if(form.is_valid()):
            form.save()
        return JsonResponse({"status" : "SUCCESS"})

@login_required(login_url="autentikasi:loginPage")
def get_all(request):
    reviews = Review.objects.all()
    result = []
    for obj in reviews:
        result.append({
            "username":str(obj.username), 
            "rating":obj.rating, 
            "review":obj.review,
            "mobil":Mobil.objects.get(id=obj.mobil.id).foto,
            "id":obj.id})
    return JsonResponse({"result":result, "user" : str(request.user)})


@login_required(login_url="autentikasi:loginPage")
def delete(request):
    if(request.method == "POST"):
        review = Review.objects.get(id=request.POST['car_id'])
        review.delete()
        return JsonResponse({"status" : "SUCCESS"})